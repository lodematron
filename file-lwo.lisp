
(in-package :lodematron)


;; top level iff forms (groups)
(defconstant +form-group-id+ (string-id "FORM"))
(defconstant +cat-group-id+ (string-id "CAT "))
(defconstant +list-group-id+ (string-id "LIST"))

(defparameter *iff-group-contents-parsers* (make-hash-table))
(defparameter *lwob-chunk-parsers* (make-hash-table))
(defparameter *lwob-subchunk-parsers* (make-hash-table))

;; lwob group header
(defconstant +lwob-group-type+ (string-id "LWOB"))

(defclass iff-group ()
  ((group-id :accessor group-id-of)
   (group-size :accessor group-size-of)
   (group-type :accessor group-type-of))
  (:documentation "IFF Top level grouping"))


(defmethod initialize-instance :after ((self iff-group) &key stream)
  "Create a group from a stream"
  (align-for-read stream +word-align+)
  (setf (group-id-of self) (read-value 'u32 stream :endian :big))
  (setf (group-size-of self) (read-value 'u32 stream :endian :big))
  (setf (group-type-of self) (read-value 'u32 stream :endian :big)))

(defgeneric group-size (group))

(defmethod group-size ((group iff-group)) 
  "Return the number of bytes to read after reading the group header."
  (- (group-size-of group) 4))

(defconstant +lwob-pnts-id+ (string-id "PNTS"))
(defconstant +lwob-srfs-id+ (string-id "SRFS"))
(defconstant +lwob-pols-id+ (string-id "POLS"))
(defconstant +lwob-surf-id+ (string-id "SURF"))

;; chunks -- groups contain chunks
(defclass iff-chunk ()
  ((chunk-id :accessor chunk-id-of)
   (chunk-size :accessor chunk-size-of))
  (:documentation "IFF file chunk"))

(defmethod initialize-instance :after ((self iff-chunk) &key stream)
  "Create a iff chunk and read it in."
  (align-for-read stream +word-align+)
  (format *debug-io* "Chunk At ~X " (file-position stream))
  (setf (chunk-id-of self) (read-value 'u32 stream :endian :big))
  (setf (chunk-size-of self) (read-value 'u32 stream :endian :big)))

(defconstant +lwob-colr-id+ (string-id "COLR"))
(defconstant +lwob-flag-id+ (string-id "FLAG"))
(defconstant +lwob-lumi-id+ (string-id "LUMI"))
(defconstant +lwob-diff-id+ (string-id "DIFF"))
(defconstant +lwob-spec-id+ (string-id "SPEC"))
(defconstant +lwob-refl-id+ (string-id "REFL"))
(defconstant +lwob-tran-id+ (string-id "TRAN"))
(defconstant +lwob-timg-id+ (string-id "TIMG"))
(defconstant +lwob-tflg-id+ (string-id "TFLG"))
(defconstant +lwob-tsiz-id+ (string-id "TSIZ")) 
(defconstant +lwob-tctr-id+ (string-id "TCTR")) 
(defconstant +lwob-tfal-id+ (string-id "TFAL")) 
(defconstant +lwob-tvel-id+ (string-id "TVEL"))
(defconstant +lwob-tclr-id+ (string-id "TCLR"))
(defconstant +lwob-tval-id+ (string-id "TVAL"))
(defconstant +lwob-tamp-id+ (string-id "TAMP"))
(defconstant +lwob-tfrq-id+ (string-id "TFRQ"))
(defconstant +lwob-tsp0-id+ (string-id "TSP0"))
(defconstant +lwob-tsp1-id+ (string-id "TSP1"))
(defconstant +lwob-tsp2-id+ (string-id "TSP2"))
(defconstant +lwob-ctex-id+ (string-id "CTEX"))
(defconstant +lwob-dtex-id+ (string-id "DTEX"))
(defconstant +lwob-glos-id+ (string-id "GLOS"))

(defclass iff-subchunk ()
  ((subchunk-id :accessor subchunk-id-of)
   (subchunk-size :accessor subchunk-size-of))
  (:documentation "IFF file subchunk"))
  
(defmethod initialize-instance  :around ((self iff-subchunk) &key stream)
  "Create a iff sub chunk and read it in."
  (format *debug-io* "Sub Chunk At ~X " (file-position stream))
  (setf (subchunk-id-of self) (read-value 'u32 stream :endian :big))
  (setf (subchunk-size-of self) (read-value 'u16 stream :endian :big)))


(defgeneric iff-group-parser (group in-stream out-fn))

;; there are three kinds of top level groups in an iff file
;; FORM -- which is raw chunked data 
;; LIST and CAT -- which are index collections of chunks or references to chunks
;; for LWO we only worry about FORMS
(defmethod iff-group-parser ((group iff-group) in-stream out-fn)
  "Parse an iff group, read from in-stream, dump to out-fn."
  (labels 
      ((skip-group ()
                   (format *debug-io* "~&Skipping unknown group ~A ~A ~T ~X" 
                           (id-string (group-id-of group))
                           (id-string (group-type-of group))
                           (group-size-of group))
                   (advance-file-position in-stream (group-size group))))
    (cond 
      ((= (group-id-of group) +form-group-id+)
       (progn "~&Found form Group")
       (funcall (gethash (group-type-of group) *iff-group-contents-parsers*) group in-stream out-fn))
      ((= (group-id-of group) +cat-group-id+) 
       (skip-group))
      ((= (group-id-of group) +list-group-id+) 
       (skip-group)))))


(defmacro def-group-contents-parser (form-type (group-sym in-stream-sym out-fn-sym) &rest body)
  "Declare a parser for the chunks in an iff  group"
  `(setf (gethash ,form-type *iff-group-contents-parsers*) 
        (lambda (,group-sym ,in-stream-sym ,out-fn-sym)
          ,@body)))


(defun unknown-iff-chunk-parser (chunk in-stream out-fn)
  "Fallback for an unknown chunk"
  (declare (ignore out-fn))
  (format *debug-io* "~&Skipping ~A chunk of ~X bytes"
          (id-string  (chunk-id-of chunk))
          (chunk-size-of chunk))
  (advance-file-position in-stream (chunk-size-of chunk)))

(defmacro def-lwob-chunk-parser (id (chunk-sym in-sym out-sym) &body body)
  "Create a parser to parse a chunk inside an LWOB group"
  `(setf (gethash ,id *lwob-chunk-parsers*) 
         (lambda (,chunk-sym ,in-sym ,out-sym)
           (declare (ignorable ,chunk-sym ,in-sym ,out-sym))
           ,@body)))


(defmacro def-lwob-subchunk-parser (id (subchunk-sym in-sym out-sym) &body body)
  "Create a parser to parse a chunk inside an LWOB group"
  `(setf (gethash ,id *lwob-subchunk-parsers*) 
         (lambda (,subchunk-sym ,in-sym ,out-sym)
           (declare (ignorable ,subchunk-sym ,in-sym ,out-sym))
           ,@body)))


(defun parse-groups (in-stream out-fn)
  "Top level IFF File parsing function"
  (iterate 
    (while (<  (file-position in-stream) (file-length in-stream)))
    (for group = (make-instance 'iff-group :stream in-stream))
    (format *debug-io* "~& At ~X " (file-position in-stream))
    (iff-group-parser group in-stream out-fn)))


;; "Parse the contents of a lwob group"
(def-group-contents-parser +lwob-group-type+ (group in-stream out-fn)        
  ;; BEWARE group-end is captured here -- this is unhygenic.
  (let ((group-end  (+ (file-position in-stream) (group-size group))))
    (format *debug-io* "Parsing LWOB Group that ends at ~X%" group-end)
    (iterate
      (while (< (file-position in-stream) group-end))
      (for chunk = (make-instance 'iff-chunk :stream in-stream))
      (format *debug-io* "~&Found ~A " (id-string (chunk-id-of chunk)))
      (funcall 
       (gethash (chunk-id-of chunk) *lwob-chunk-parsers* #'unknown-iff-chunk-parser) 
       chunk in-stream out-fn))))

;; Parsers for individual chunks inside an lwob group

;; PNTS chunk == vertices
(def-lwob-chunk-parser +lwob-pnts-id+ (chunk in-stream out-fn)
  (let ((chunk-end (+ (file-position in-stream) (chunk-size-of chunk))))
    (iterate
     (while (< (file-position in-stream) chunk-end))
     (let* ((x (read-value 'float32 in-stream :endian :big) )
            (y (read-value 'float32 in-stream :endian :big))
            (z (read-value 'float32 in-stream :endian :big)))
       (format *debug-io* "Point ~A ~A ~A~& " x y z)))))

    
;; POLS chunk == polygons
(def-lwob-chunk-parser +lwob-pols-id+ (chunk in-stream out-fn)
  (labels 
      ((parse-polygon ()                      
         (let* ((vertex-count (logand #X3FF (read-value 'u16 in-stream :endian :big)))
                (vertices (make-array (list vertex-count) :element-type '(unsigned-byte 16))))
           (format *debug-io* "Parsing polygon with ~A vertices~&" vertex-count)
           (iterate
             (for vertex from 0 below vertex-count)
             (setf (aref vertices vertex) (read-value 'u16 in-stream :endian :big)))
           vertices)))
    (let ((chunk-end (+ (file-position in-stream) (chunk-size-of chunk))))
      (iterate
       (while (< (file-position in-stream) chunk-end))
       (let ((polygon (parse-polygon)))
         (declare (ignorable polygon))
         (when (< (file-position in-stream) chunk-end)
           (let ((surface (read-value 'u16 in-stream :endian :big)))
             (when (< surface 0)     
               (let ((detail-count (read-value 's16 in-stream :endian :big)))
                 (format *debug-io* "~A Details.." detail-count)
                 (iterate 
                   (for detail-index from 0 below detail-count)
                   (parse-polygon)))))))))))

;; SRFS chunk == surface names
(def-lwob-chunk-parser +lwob-srfs-id+ (chunk in-stream out-fn)
  (let ((chunk-end (+ (file-position in-stream) (chunk-size-of chunk))))
    (format *debug-io* "Surface chunk~&")
    (iterate (while (< (file-position in-stream) chunk-end))
             (format *debug-io* "String at ~X~&" (file-position in-stream))
             (let ((surf-name (read-value 'asciiz in-stream)))
               (align-for-read in-stream +word-align+)
               (format *debug-io* "Surface ~A~&" surf-name)))))

;; SURF chunk == surface properties
(def-lwob-chunk-parser +lwob-surf-id+ (chunk in-stream out-fn)
  (labels ((skip-subchunk (subchunk in-stream)
             (format *debug-io* "~&Skipping unknown subchunk ~A " 
                     (id-string (subchunk-id-of subchunk)))
             (advance-file-position in-stream (subchunk-size-of subchunk))))
      (let ((chunk-end (+ (file-position in-stream) (chunk-size-of chunk)))
            (surface-name (read-value 'asciiz in-stream)))
        (format *debug-io* "~&For surface ~A " surface-name)
        (align-for-read in-stream +word-align+)
        (iterate 
          (while (< (file-position in-stream) chunk-end))
          (let* ((subchunk (make-instance 'iff-subchunk :stream in-stream))
                 (parser (gethash (subchunk-id-of subchunk) *lwob-subchunk-parsers*)))
            (if parser
                (funcall parser subchunk in-stream out-fn)
                (skip-subchunk subchunk in-stream)))))))

(def-lwob-subchunk-parser +lwob-colr-id+ (chunk in-stream out-fn)
  (let* ((red (read-value 'u8 in-stream))
         (green (read-value 'u8 in-stream))
         (blue (read-value 'u8 in-stream))
         (dummy (read-value 'u8 in-stream)))
    (declare (ignorable dummy))
    (format *debug-io* "Colour ~X ~X ~X " red green blue)))

(def-lwob-subchunk-parser +lwob-lumi-id+ (chunk in-stream out-fn)
  (let ((luminosity (read-value 'u16 in-stream)))
    (format *debug-io* "Luminosity ~A " (/ luminosity 256))))

(def-lwob-subchunk-parser +lwob-diff-id+ (chunk in-stream out-fn)
  (let ((diffusion (read-value 'u16 in-stream)))
    (format *debug-io* "Diffusion ~A " (/ diffusion 256))))

(def-lwob-subchunk-parser +lwob-spec-id+ (chunk in-stream out-fn)
  (let ((specularity (read-value 'u16 in-stream)))
    (format *debug-io* "Specularity ~A " (/ specularity 256))))

(def-lwob-subchunk-parser +lwob-refl-id+ (chunk in-stream out-fn)
  (let ((reflectivity (read-value 'u16 in-stream)))
    (format *debug-io* "Reflectivity ~A " (/ reflectivity 256))))

(def-lwob-subchunk-parser +lwob-tran-id+ (chunk in-stream out-fn)
  (let ((transparency (read-value 'u16 in-stream)))
    (format *debug-io* "Transparency ~A " (/ transparency 256))))

(def-lwob-subchunk-parser +lwob-glos-id+ (chunk in-stream out-fn)
  (let ((glossiness (read-value 'u16 in-stream)))
    (format *debug-io* "Glossiness ~A " (/ glossiness 1024))))

(defparameter *texture-context* :colour-texture)

(def-lwob-subchunk-parser +lwob-ctex-id+ (chunk in-stream out-fn)
  (let ((colour-texture (read-value 'asciiz in-stream)))
    (align-for-read in-stream +word-align+)
    (setf *texture-context* :colour-texture)
    (format *debug-io* "Colour texture ~A " colour-texture)))

(def-lwob-subchunk-parser +lwob-dtex-id+ (chunk in-stream out-fn)
  (let ((diffuse-texture (read-value 'asciiz in-stream)))
    (align-for-read in-stream +word-align+)
    (setf *texture-context* :diffuse-texture)
    (format *debug-io* "Diffuse texture ~A " diffuse-texture)))

(def-lwob-subchunk-parser +lwob-timg-id+ (chunk in-stream out-fn)
  (let ((texture-name (read-value 'asciiz in-stream)))
    (align-for-read in-stream +word-align+)
    (format *debug-io* "Texture fname ~A" texture-name)))

(def-lwob-subchunk-parser +lwob-tflg-id+ (chunk in-stream out-fn)
  (let ((texture-flags (read-value 'u16 in-stream)))
    (destructure-bits texture-flags (xaxis yaxis zaxis world negative pixel-blending antialiasing)
      (format *debug-io* "axes ~A ~A ~A" xaxis yaxis zaxis)
      (format *debug-io* "world ~A" world)
      (format *debug-io* "negative ~A" negative))))

(def-lwob-subchunk-parser +lwob-tsiz-id+ (chunk in-stream out-fn)
  (let ((x (read-value 'float32 in-stream))
        (y (read-value 'float32 in-stream))
        (z (read-value 'float32 in-stream)))
    (format "Size x y z ~A ~A ~A " x y z)))

(def-lwob-subchunk-parser +lwob-tctr-id+ (chunk in-stream out-fn)
  (let ((x (read-value 'float32 in-stream))
        (y (read-value 'float32 in-stream))
        (z (read-value 'float32 in-stream)))
    (format *debug-io* "Centre x y z ~A ~A ~A " x y z)))

(def-lwob-subchunk-parser +lwob-tfal-id+ (chunk in-stream out-fn)
  (let ((x (read-value 'float32 in-stream))
        (y (read-value 'float32 in-stream))
        (z (read-value 'float32 in-stream)))
    (format *debug-io* "Falloff x y z ~A ~A ~A " x y z)))

(def-lwob-subchunk-parser +lwob-tvel-id+ (chunk in-stream out-fn)
  (let ((x (read-value 'float32 in-stream))
        (y (read-value 'float32 in-stream))
        (z (read-value 'float32 in-stream)))
    (format *debug-io* "Velocity x y z ~A ~A ~A " x y z)))

(def-lwob-subchunk-parser +lwob-tclr-id+ (chunk in-stream out-fn)
  (let* ((red (read-value 'u8 in-stream))
         (green (read-value 'u8 in-stream))
         (blue (read-value 'u8 in-stream)))
    (read-value 'u8 in-stream)
    (format *debug-io* "Colour ~X ~X ~X " red green blue)))

(defun parse-iff-file (input-file)
  (let ((result (make-mesh 'simple-mesh))
        (result-mesh (gethash result *meshes*)))
  (with-open-file
      (in-stream input-file :element-type '(unsigned-byte 8))
    (parse-groups in-stream result-mesh))))






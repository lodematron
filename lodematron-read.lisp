

(in-package :lodematron)

 ;; -- reading atomic binary values  -----------------------------------------

(defgeneric read-value (type stream &key array-size alignment endian)
  (:documentation "Read a value of the given type from the file."))

;; -- read aligned binary data  ----------------------------------------------

(defgeneric align-for-read (stream alignment))

(defconstant +word-align+  1)
(defconstant +dword-align+ 3)
(defconstant +qword-align+ 7)

;; why am I advancing this byte by byte?
(defmethod align-for-read ((self stream) alignment)
  "Align a file stream for reading at alignment bytes boundary."
  (when (not (zerop alignment))
    (iterate
     (until (zerop (logand (file-position self) alignment)))
     (file-position self (1+ (file-position self))))))

;; -- read arrays of binary data ---------------------------------------------
;; -- note the lack of hygine with regards to "endian" 

(defmacro with-size-and-alignment-read (array-element-type alignment array-size &rest body)
  "Use the function body to read a value into an array of the given
size, begnning the read at the given alignment. Assumes the read will
be from a variable bound to a symbol called self, and alingment is
performed by calling (align-for-read self bytes)."
  (once-only (alignment array-size)
             (with-gensyms (read-once result)
               `(labels ((,read-once ()
                          ,@body))
                 (when ,alignment
                   (align-for-read self alignment))
                 (if (not (zerop ,array-size))
                     (let ((,result (make-array ,array-size :element-type ',array-element-type)))
                       (loop
                          for index from 0 below ,array-size
                          do
                            (setf (aref ,result index) (,read-once)))
                       ,result) 
                     (,read-once))))))


(defmethod read-value ((type (eql :u8)) (self stream) &key (array-size 0) (alignment 0) (endian :little))
  (declare (ignore endian))
  (with-size-and-alignment-read 
      (unsigned-byte 8) alignment array-size
      (read-byte self)))


(defmethod read-value ((type (eql :s8)) (self stream) &key (array-size 0) (alignment 0) (endian :little))
  (declare (ignore endian))
  (with-size-and-alignment-read 
      (unsigned-byte 8) alignment array-size
      (let ((u8 (read-value :u8 self)))
        (if (> u8 #X7F)
            (- u8 #X100)
            u8))))

(defmethod read-value ((type (eql :u16)) (self stream) &key (array-size 0) (alignment 0) (endian :little))
  (with-size-and-alignment-read 
      (unsigned-byte 16) alignment array-size
      (ccase endian
        (:little
         (let ((u16 0))
           (setf (ldb (byte 8 0) u16) (read-byte self))
           (setf (ldb (byte 8 8) u16) (read-byte self))
          u16))
        (:big
         (let ((u16 0))
           (setf (ldb (byte 8 8) u16) (read-byte self))
           (setf (ldb (byte 8 0) u16) (read-byte self))
           u16)))))

(defmethod read-value ((type (eql :s16)) (self stream) &key (array-size 0) (alignment 0) (endian :little))
  (with-size-and-alignment-read 
      (unsigned-byte 16) alignment array-size
      (let ((u16 (read-value :u16 self :endian endian)))
        (if (> u16 #X7FFF)
            (- u16 #X10000)
            u16))))

(defmethod read-value ((type (eql :u32)) (self stream) &key (array-size 0) (alignment 0) (endian :little))
  (with-size-and-alignment-read 
      (unsigned-byte 32) alignment array-size 
      (ccase endian
        (:little 
         (let ((u32 0))
           (setf (ldb (byte 8 0) u32) (read-byte self))
           (setf (ldb (byte 8 8) u32) (read-byte self))
           (setf (ldb (byte 8 16) u32) (read-byte self))
           (setf (ldb (byte 8 24) u32) (read-byte self))
           u32))
        (:big
         (let ((u32 0))
           (setf (ldb (byte 8 24) u32) (read-byte self))
           (setf (ldb (byte 8 16) u32) (read-byte self))
           (setf (ldb (byte 8 8 ) u32) (read-byte self))
           (setf (ldb (byte 8 0) u32) (read-byte self))
           u32)))))

(defmethod read-value ((type (eql :s32)) (self stream) &key ( array-size 0 ) ( alignment 0 ) (endian :little))
  (with-size-and-alignment-read 
      (unsigned-byte 32) alignment array-size
       (let ((u32 (read-value :u32 self :endian endian)))
        (if (> u32 #X7FFFFFFF)
            (- u32 #X100000000)
            u32))))

(defmethod read-value ((type (eql :u64)) (self stream) &key ( array-size 0 ) ( alignment 0 ) (endian :little))
  (with-size-and-alignment-read 
      (unsigned-byte 64) alignment array-size 
      (ccase endian
        (:little 
         (let ((u64 0))
          (setf (ldb (byte 8 0) u64) (read-byte self))
          (setf (ldb (byte 8 8) u64) (read-byte self))
          (setf (ldb (byte 8 16) u64) (read-byte self))
          (setf (ldb (byte 8 24) u64) (read-byte self))
          (setf (ldb (byte 8 32) u64) (read-byte self))
          (setf (ldb (byte 8 40) u64) (read-byte self))
          (setf (ldb (byte 8 48) u64) (read-byte self))
          (setf (ldb (byte 8 56) u64) (read-byte self))
          u64))
        (:big 
        (let ((u64 0))
          (setf (ldb (byte 8 56) u64) (read-byte self))
          (setf (ldb (byte 8 48) u64) (read-byte self))
          (setf (ldb (byte 8 40) u64) (read-byte self))
          (setf (ldb (byte 8 32) u64) (read-byte self))
          (setf (ldb (byte 8 24) u64) (read-byte self))
          (setf (ldb (byte 8 16) u64) (read-byte self))
          (setf (ldb (byte 8 8 ) u64) (read-byte self))
          (setf (ldb (byte 8 0) u64) (read-byte self))
          u64)))))

(defmethod read-value ((type (eql :s64)) (self stream) &key ( array-size 0 ) ( alignment 0 ) (endian :little))
  (with-size-and-alignment-read 
      (unsigned-byte 32) alignment array-size
      (let ((u64 (read-value :u64 self :endian endian)))
        (if (> u64 #X7FFFFFFFFFFFFFFF)
            (- u64 #X10000000000000000)
            u64))))

(defmethod read-value ((type (eql :float32))  (self stream) &key ( array-size 0 ) ( alignment 0 ) (endian :little))
  (with-size-and-alignment-read
      (unsigned-byte 32) alignment array-size
      (let ((u32 (read-value :u32 self :endian endian)))
        (ieee-floats::decode-float32 u32))))

(defmethod read-value ((type (eql :float64))  (self stream) &key ( array-size 0 ) ( alignment 0 ) (endian :little))
  (with-size-and-alignment-read
      (unsigned-byte 32) alignment array-size
      (let ((u64 (read-value :u64 self :endian endian)))
        (ieee-floats::decode-float64 u64))))

(defmethod read-value ((type (eql :asciiz)) (self stream) &key ( array-size 0 ) ( alignment 0 ) (endian :little))
  "Read a number of zero terminated ascii strings from the stream"
  (declare (ignorable endian))
  (with-size-and-alignment-read
      t 0 alignment array-size
      (let ((result (make-array '(0) :element-type 'base-char :fill-pointer 0 :adjustable t)))
        (iterate
          (for byte = (read-byte self))
          (until (zerop byte)) 
          (vector-push-extend (code-char byte) result)) ;; to do - a byte isn't a char, use octets-to-string?
        result)))

(defmethod read-value ((type (eql :nstring32))  (self stream) &key ( array-size 0 ) ( alignment 0 ) (endian :little))
  "Read a string preceeded with a 32 bit length from the stream"
  (declare (ignorable endian))
  (with-size-and-alignment-read
      t alignment array-size
      (let ((result (make-array '(0) :element-type 'base-char :fill-pointer 0 :adjustable t)))
        (let ((string-length (read-value :u32 self :array-size 0 :alignment alignment :endian endian)))
          (iterate
            (for index from 0 below string-length)
            (for byte = (read-byte self))
            (vector-push-extend (code-char byte) result))
          result))))

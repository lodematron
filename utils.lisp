
(in-package :lodematron)

;; utils used throughout the code


(defmacro with-gensyms ((&rest names) &body forms)
  "Create hygenic tempoary variables for macros"
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@forms))

(defmacro once-only ((&rest names) &body body)
  "Evaluate form once only and assign to temp var for macro body"
  (let ((gensyms (loop for n in names collect (gensym))))
    `(let (,@(loop for g in gensyms collect `(,g (gensym))))
      `(let (,,@(loop for g in gensyms for n in names collect ``(,,g ,,n)))
        ,(let (,@(loop for n in names for g in gensyms collect `(,n ,g)))
           ,@body)))))

(defmacro destructure-bits (bits vals &body forms)
  "Destructure a numeric variable into individual bits"
  (once-only (bits)
    `(let 
         ,(loop
             for val in vals
             for mask = 1 then (ash mask 1)
             collect `(,val (logandc2 ,bits ,mask)))
       (declare (ignorable ,@vals))
       ,@forms)))


(defun make-extensible-string (dimensions &key (initial-element #\Space))
  "Create an ajustable string"
  (make-array dimensions
              :element-type 'character
              :initial-element initial-element
              :adjustable t
              :fill-pointer dimensions))

(defmacro indefinitely ((var form) &body body)
  "Expand to a form that repeatedly binds var to form and evaluates body until
   something goes wrong (like a condition being signalled)"
  `(do ((,var ,form ,form))
       (nil)
    ,@body))


(defun extend-string (elements  string)
  "Add the elements to the end of the extensitble  string."
  (assert (adjustable-array-p  string))
  (labels
      ((extend-string-with-string (chars string)
         (iterate
          (for char in-vector chars)
          (vector-push-extend char string))
         string))
    (typecase elements
      (character (vector-push-extend elements string))
      (symbol (extend-string-with-string (string elements) string))
      (string (extend-string-with-string elements string))
      (number (extend-string-with-string (format nil "~D" elements) string))
      (list (mapcar #'(lambda (x)
                        (extend-string x string))
                    elements))))
  string)

(defun collapse-string (stringlist)
  "Given a list of strings collapse it to a single string."
  (let ((result (make-extensible-string 0)))
    (iterate
     (for string in stringlist)
     (iterate
      (for char in-vector string)
      (vector-push-extend char result)))
    result))


(defmacro formatting (stream initial-string &rest args)
"Format replacement using keywords rather than a control string."
(labels
    ((add-numeric-control (control-char control-keys format-string)
       (extend-string #\~ format-string)
       (destructuring-bind
             (&key width fillchar form)
           control-keys
         (declare (ignore form))
         (when width
           (extend-string width format-string)
           (when fillchar
             (extend-string #\, format-string)
             (extend-string #\' format-string)
             (extend-string fillchar format-string)))
         (extend-string control-char format-string)))
     (process-control-item (control)
       (let ((format-string (make-extensible-string 0)))
         (typecase control
           (string
            (extend-string control format-string))
           ;; handle simle forms
           (symbol
            (case control
              (:tab (extend-string "~T" format-string))
              (:binary (extend-string "~B" format-string))
              (:octal (extend-string "~O" format-string))
              (:decimal (extend-string "~D" format-string))
              (:hex (extend-string "~X" format-string))
              (:newline (extend-string "~%" format-string))
              (:freshline (extend-string "~&" format-string))
              (:page (extend-string "~|" format-string))
              (:pretty (extend-string "~A" format-string))
              (:readable (extend-string "~S" format-string))
              (:write (extend-string "~W" format-string))))
           (list
            ;; handle complex forms
            (case (first control)
              (:binary (add-numeric-control #\B (rest control) format-string))
              (:octal (add-numeric-control #\O (rest control) format-string))
              (:decimal (add-numeric-control #\D (rest control) format-string))
              (:hex (add-numeric-control #\X (rest control) format-string))))
           (number
            (extend-string control format-string))
           (t nil))
         format-string))
     (process-argument-item (control)
       (typecase control
         (symbol (when (not (keywordp control)) control))
         (list (car (last control)))
         (t nil))))
  `(format ,stream
           ,(collapse-string (cons initial-string (mapcar   #'process-control-item args)))
           ,@(remove-if #'null (mapcar #'process-argument-item args)))))


(defun pad2 (i)
  (logandc1 #X1 (1+ i)))

(defun string-id (string)
  "Convert string into 4byte IFFF id"
  (logior 
   (ash  (char-code  (char string 3)) 0)
   (ash  (char-code  (char string 2)) 8)
   (ash  (char-code  (char string 1)) 16)
   (ash  (char-code   (char string 0)) 24)))

(defun id-string (id)
  "Convert 4 byte IFFF id into string"
  (concatenate 'string
               (string (code-char (logand #XFF (ash id -24))))
               (string (code-char (logand #XFF (ash id -16))))
               (string (code-char (logand #XFF  (ash id -8)))) 
               (string (code-char (logand #XFF id)))))


(in-package :lodematron)


;; buffering binary files --------------------

;; read and write an entire file to memory in one go
(defun read-file-to-usb8-array (filepath)
  "Opens a reads a file. Returns the contents as single unsigned-byte array"
  (with-open-file (in filepath :direction :input :element-type '(unsigned-byte 8))
    (let* ((file-len (file-length in))
           (usb8 (make-array file-len :element-type '(unsigned-byte 8)))
           (pos (read-sequence usb8 in)))
      (unless (= file-len pos)
        (error "Length read (~D) doesn't match file length (~D)~%" pos file-len))
      usb8)))

(defun write-usb8-array-to-file (usb8 file)
  "Opens a reads a file. Returns the contents as single unsigned-byte array"
  (with-open-file (out file :direction :output 
                       :if-exists :supersede 
                       :if-does-not-exist :create 
                       :element-type '(unsigned-byte 8))
    (let* ((pos (length (write-sequence usb8 out)))
           (file-len (file-length out)))
      (unless (= file-len pos)
        (error "Length written (~D) doesn't match file length (~D)~%" pos file-len))
      usb8)))

;; class to hold file data in memory
(defclass binary-file-data ()
  ((buffered-data :accessor buffered-data-of)
   (buffer-pos :accessor buffer-pos-of :initform 0))
  ( :documentation "Binary file held in memory with accompanying seek position"))

;; initalise buffer via reading a file
(defmethod initialize-instance :after ((self binary-file-data) &key filepath)
  "Initalise a buffered via reading the lot into memory."
  (when filepath (setf (buffered-data-of self)
                       (read-file-to-usb8-array filepath))))

;; writing entire files
(defgeneric	write-binary-file (binary-file-data file)
  (:documentation "Write the buffered data to the binary file"))
		       
(defmethod  write-binary-file ((self binary-file-data) file)
  "Write the buffered data to the binary file"
  (write-usb8-array-to-file 
   (buffered-data-of self)
   file))




(in-package :lodematron)

(defun has-more (stream)
  (< (file-position stream) (file-length stream)))

(defun parse-ifs-file (input-file output-fn)
  (declare (ignorable output-fn))
  (with-open-file 
      (input input-file :element-type '(unsigned-byte 8))
    (let 
        ((id-string (read-value 'nstring32 input)))
      (cond
        ;; is it a binary ifs?
        ((string= id-string "IFS ")
         ;; 'tis a binary ifs
         (let 
             ((ifs-version (read-value 'float32 input))
              (ifs-name (read-value 'nstring32 input)))
           (declare (ignorable ifs-version ifs-name))
           (funcall output-fn :mesh-name ifs-name)
           (iterate
             (while (has-more input))
             (let ((id-string (read-value 'nstring32 input)))
               (cond
                 ((string= id-string "VERTICES ")
                  (let ((nvertices (read-value 'u32 input)))
                    (iterate
                      (for i from 0 below nvertices)
                      (let* ((x (read-value 'float32 input))
                             (y (read-value 'float32 input))
                             (z (read-value 'float32 input)))
                        (funcall output-fn :add-vertex  (make-vertex3d*  x  y z 1.0))))))
                 ((string= id-string "TRIANGLES ")
                  (let ((ntriangles (read-value 'u32 input)))
                    (iterate
                      (for i from 0 below ntriangles)
                      (let* ((a (read-value 'u32 input))
                             (b (read-value 'u32 input))
                             (c (read-value 'u32 input)))
                        (funcall output-fn :add-triangle (make-triangle* a b c))))))
                 ((string= id-string "TEXTURECOORD ")
                  (let ((ntexcoords (read-value 'u32 input)))
                    (iterate 
                      (for i from 0 below ntexcoords)
                      (let* ((u (read-value 'float32 input))
                             (v (read-value 'float32 input)))
                        (funcall output-fn :add-texcoord (make-vector2d* u v))))))
                 (t (error "Undefined file format ~A " id-string)))))))))))



(asdf:oos 'asdf:load-op 'mixamesh)
(asdf:oos 'asdf:load-op 'lodematron)

(in-package :lodematron)


;; test code

(defparameter *md2-file* (open (merge-pathnames #P"dalekx/tris.md2") 
                               :direction :input 
                               :element-type '(unsigned-byte 8)))

(defparameter *header* (read-value :md2-header *md2-file*))

(defparameter *uvs* (parse-md2-uvs (uv-offset-of *header*) (nuvs-of *header*) *md2-file*))

(defparameter *tris* (parse-md2-triangles (tris-offset-of *header*) (ntris-of *header*) *md2-file*))

(defparameter *frames* (parse-md2-frames (frames-offset-of *header*) 
                                          (nframes-of *header*) 
                                          (framesize-of *header*) 
                                          (nvertices-of *header*) *md2-file*))


(format *debug-io* "~D Skins ~D Vertices ~D UVS ~D Tris" 
         (nskins-of *header*) 
         (nvertices-of *header*) 
         (nuvs-of *header*) 
         (ntris-of *header*))

(defparameter *test-mesh* (make-instance 'simple-mesh))

(defparameter *test-md2* (parse-md2-file *md2-file*))


(pose *test-md2* *test-mesh* "dalek042" "brit")
(repose *test-md2* *test-mesh* "dalek043" "someskin")

(close *md2-file*)

(defparameter *pcx-file* )

(defparameter *pcx-header* (read-value :pcx-header *pcx-file*))

(defparameter *pcx-data* (decode-8bit *pcx-file* (bytes-per-line-of *pcx-header*) (- (ymax-of *pcx-header*) (ymin-of *pcx-header*))))

(defparameter *pcx-palette* (extract-palette *pcx-file*))

(parse-pcx-file *pcx-file*)

(close *pcx-file*)


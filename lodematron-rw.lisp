;;;; Silly emacs, this is -*- Lisp -*-

(in-package :lodematron)

;; define-binary-class ------------------------------------------------------
;; Heavily influenced by Practical Common Lisp by Peter Seibel

(defun as-keyword (sym) 
  (intern (string sym) :keyword))

(defun as-accessor (sym) 
  (intern (concatenate 'string (string sym) "-OF")))

(defun slot->defclass-slot (spec)
  "Map a define-binary-class slot spec to a clos slot spec"
  (let ((name (first spec)))
    `(,name :initarg ,(as-keyword name) :accessor ,(as-accessor name))))

(defun mklist (x) 
  "Return the argument as a list if it isn't already"
  (if (listp x) x (list x)))

(defun normalize-slot-spec (spec)
  (list (first spec) (mklist (cdr spec))))

(defun slot->read-value (spec file-data)
  (destructuring-bind (name (type &rest args)) (normalize-slot-spec spec)
    `(setf ,name (read-value ,(as-keyword type) ,file-data ,@args))))

(defun slot->write-value (spec file-data)
  (destructuring-bind (name (type &rest args)) (normalize-slot-spec spec)
    `(write-value ,(as-keyword type) ,file-data ,name ,@args)))



(defmacro define-binary-class (name slots)
  "Define a class that can be read or written to a flat binary file."
  (with-gensyms (typevar objectvar binary-data-var) 
    `(progn
       ;; generate a defclass form
       (defclass ,name ()
         ,(mapcar #'slot->defclass-slot slots))
       ;; generate a method to read all slots
       (defmethod read-value ((,typevar (eql,(as-keyword name))) ,binary-data-var &key (alignment 0) (array-size 0) (endian :little))
         (declare (ignore endian))
         (assert (= array-size 0))
         (align-for-read ,binary-data-var alignment)
         (let ((,objectvar (make-instance ',name)))
           (with-slots ,(mapcar #'first slots) ,objectvar
             ;; note - doesn't pass on endianness or allow alignment of slots yet -- to do
             ,@(mapcar #'(lambda (x) (slot->read-value x binary-data-var)) slots))
           ,objectvar))
        ;; generate a method to write all slots
       (defmethod write-value ((,typevar (eql ,(as-keyword name))) ,binary-data-var ,objectvar &key (alignment 0) (array-size 0) (endian :little))
         (declare (ignore endian))
         (assert (= array-size 0))
         (align-for-write ,binary-data-var alignment)
         (with-slots ,(mapcar #'first slots) ,objectvar
           ;; note - doesn't pass on endianness or allow alignment of slots yet -- to do
           ,@(mapcar #'(lambda (x) (slot->write-value x binary-data-var)) slots))
         (values)))))

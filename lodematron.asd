
(in-package :asdf)

(asdf:defsystem :lodematron
  :depends-on ( :iterate :ieee-floats :mixamesh :babel :cl-fad )
  :serial t
  :components 
  ((:file "package")  
   (:file "utils")
   (:file "lodematron-binary")
   (:file "lodematron-read" )  
   (:file "lodematron-write" ) 
   (:file "lodematron-rw" )
   (:file "file-3ds")
   (:file "file-pcx")
   (:file "file-md2")
   (:file "file-ifs")
   (:file "file-lwo"))) 



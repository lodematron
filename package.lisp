;;;; package.lisp
(in-package :cl-user)

(defpackage :lodematron
  (:use :cl :cl-tuples :mixamesh :ieee-floats :iterate)
  (:export #:read-value 
           #:write-value 
           #:define-binary-class 
           #:null-terminate 
           #:string-id 
           #:id-string 
           #:parse-md2-file
           #:md2-mesh
           #:uvs-tris-of
           #:uvs-of
           #:pose
           #:repose))

(in-package :lodematron)

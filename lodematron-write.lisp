
(in-package :lodematron)

;; WIP -- an array knows it's own size and element type. Perhaps we could dispatch on these?

(defgeneric write-value (type stream value &key array-size alignment endian)
  (:documentation "Read a value of the given type from the file."))

;; write aligned binary data  -----------------------------------------

(defgeneric align-for-write (stream alignment))

(defmethod align-for-write ((self stream) alignment)
  "Align a file stream for reading at alignment bytes boundary."
  (when (not (zerop alignment))
    (iterate
     (until (zerop (logand (file-position self) alignment)))
      (file-position self (1+ (file-position self))))))

  
(defmacro with-size-and-alignment-write (alignment array-size value-sym &rest body)
    (once-only (alignment array-size)
             (with-gensyms (write-once index)
               `(labels ((,write-once (,value-sym)
                          ,@body))
                 (when ,alignment
                   (align-for-write self alignment))
                 (if (not (zerop ,array-size))
                     (iterate
                        (for ,index from 0 below ,array-size)
                        (,write-once (elt ,value-sym ,index))) 
                     (,write-once ,value-sym))))))

(defmethod write-value ((type (eql :u8)) (self stream) value &key array-size (alignment 0) (endian :little))
  (declare (ignore endian))
  (with-size-and-alignment-write 
       alignment array-size value
      (write-byte value self)))

(defmethod write-value ((type (eql :s8)) (self stream) value &key array-size (alignment 0) (endian :little))
  (declare (ignore endian))
  (with-size-and-alignment-write 
       alignment array-size value
      (write-byte value self)))
  
(defmethod write-value ((type (eql :u16)) (self stream) value &key array-size alignment (endian :little))
    (with-size-and-alignment-write 
         alignment array-size value 
        (ccase endian
               (:little 
                (let ((u16 value))
                  (write-byte (ldb (byte 8 0) u16) self)
                  (write-byte (ldb (byte 8 8) u16) self)))
               (:big
                (let ((u16 value))
                  (write-byte (ldb (byte 8 8) u16) self)
                  (write-byte (ldb (byte 8 0) u16) self))))))

(defmethod write-value ((type (eql :s16)) (self stream) value &key (array-size 0) (alignment 0) (endian :little))
    (with-size-and-alignment-write 
         alignment array-size value 
        (ccase endian
               (:little 
                (let ((s16 value))
                  (write-byte (ldb (byte 8 0) s16) self)
                  (write-byte (ldb (byte 8 8) s16) self)))
               (:big
                (let ((s16 value))
                  (write-byte (ldb (byte 8 8) s16) self)
                  (write-byte (ldb (byte 8 0) s16) self))))))


(defmethod write-value ((type (eql :u32)) (self stream) value &key (array-size 0) (alignment 0) (endian :little))
    (with-size-and-alignment-write 
         alignment array-size value
        (ccase endian
          (:little
           (let ((u32 value))
             (write-byte (ldb (byte 8 0) u32) self)
             (write-byte (ldb (byte 8 8) u32) self)
             (write-byte (ldb (byte 8 16) u32) self)
             (write-byte (ldb (byte 8 24) u32) self)))
           (:big
            (let ((u32 value))
              (write-byte (ldb (byte 8 24) u32) self)
              (write-byte (ldb (byte 8 16) u32) self)
              (write-byte (ldb (byte 8 8) u32) self)
              (write-byte (ldb (byte 8 0) u32) self))))))

(defmethod write-value ((type (eql :s32)) (self stream) value &key (array-size 0) (alignment 0) (endian :little))
    (with-size-and-alignment-write 
         alignment array-size value
        (ccase endian
               (:little
                (let ((s32 value))
                  (write-byte (ldb (byte 8 0) s32) self)
                  (write-byte (ldb (byte 8 8) s32) self)
                  (write-byte (ldb (byte 8 16) s32) self)
                  (write-byte (ldb (byte 8 24) s32) self)))
               (:big
                (let ((s32 value))
                  (write-byte (ldb (byte 8 24) s32) self)
                  (write-byte (ldb (byte 8 16) s32) self)
                  (write-byte (ldb (byte 8 8) s32) self)
                  (write-byte (ldb (byte 8 0) s32) self))))))

(defmethod write-value ((type (eql :float32)) value (self stream) &key ( array-size 0 ) ( alignment 0 ) (endian :little))
  (with-size-and-alignment-write
       alignment array-size value
      (let ((u32 (ieee-floats::encode-float32 value)))
        (ccase endian
          (:little
           (write-byte (ldb (byte 8 0) u32) self)
           (write-byte (ldb (byte 8 8) u32) self)
           (write-byte (ldb (byte 8 16) u32) self)
           (write-byte (ldb (byte 8 24) u32) self))
          (:big
           (write-byte (ldb (byte 8 24) u32) self)
           (write-byte (ldb (byte 8 16) u32) self)
           (write-byte (ldb (byte 8 8) u32) self)
           (write-byte (ldb (byte 8 0) u32) self))))))

(defmethod write-value ((type (eql :u64)) (self stream) value &key (array-size 0) (alignment 0) (endian :little))
    (with-size-and-alignment-write 
        alignment array-size value
        (ccase endian
          (:little
           (let ((u64 value))
             (write-byte (ldb (byte 8 0) u64) self)
             (write-byte (ldb (byte 8 8) u64) self)
             (write-byte (ldb (byte 8 16) u64) self)
             (write-byte (ldb (byte 8 24) u64) self)
             (write-byte (ldb (byte 8 32) u64) self)
             (write-byte (ldb (byte 8 40) u64) self)
             (write-byte (ldb (byte 8 48) u64) self)
             (write-byte (ldb (byte 8 56) u64) self)))
           (:big
            (let ((u64 value))              
              (write-byte (ldb (byte 8 56) u64) self)
              (write-byte (ldb (byte 8 48) u64) self)
              (write-byte (ldb (byte 8 40) u64) self)
              (write-byte (ldb (byte 8 32) u64) self)
              (write-byte (ldb (byte 8 24) u64) self)
              (write-byte (ldb (byte 8 16) u64) self)
              (write-byte (ldb (byte 8 8) u64) self)
              (write-byte (ldb (byte 8 0) u64) self))))))

(defmethod write-value ((type (eql :s64)) (self stream) value &key (array-size 0) (alignment 0) (endian :little))
    (with-size-and-alignment-write 
         alignment array-size value
        (ccase endian
          (:little
           (let ((s64 value))
             (write-byte (ldb (byte 8 0) s64) self)
             (write-byte (ldb (byte 8 8) s64) self)
             (write-byte (ldb (byte 8 16) s64) self)
             (write-byte (ldb (byte 8 24) s64) self)
             (write-byte (ldb (byte 8 32) s64) self)
             (write-byte (ldb (byte 8 40) s64) self)
             (write-byte (ldb (byte 8 48) s64) self)
             (write-byte (ldb (byte 8 56) s64) self)))
           (:big
            (let ((s64 value))
             (write-byte (ldb (byte 8 56) s64) self)
             (write-byte (ldb (byte 8 48) s64) self)
             (write-byte (ldb (byte 8 40) s64) self)
             (write-byte (ldb (byte 8 32) s64) self)
             (write-byte (ldb (byte 8 24) s64) self)
             (write-byte (ldb (byte 8 16) s64) self)
             (write-byte (ldb (byte 8 8) s64) self)
             (write-byte (ldb (byte 8 0) s64) self))))))

(defmethod write-value ((type (eql :float64))  (self stream) value &key ( array-size 0 ) ( alignment 0 ) (endian :little))
  (with-size-and-alignment-write
       alignment array-size value
       (let ((u64 (ieee-floats::encode-float64 value)))        
        (ccase endian
          (:little
             (write-byte (ldb (byte 8 0) u64) self)
             (write-byte (ldb (byte 8 8) u64) self)
             (write-byte (ldb (byte 8 16) u64) self)
             (write-byte (ldb (byte 8 24) u64) self)
             (write-byte (ldb (byte 8 32) u64) self)
             (write-byte (ldb (byte 8 40) u64) self)
             (write-byte (ldb (byte 8 48) u64) self)
             (write-byte (ldb (byte 8 56) u64) self))
           (:big            
             (write-byte (ldb (byte 8 56) u64) self)
             (write-byte (ldb (byte 8 48) u64) self)
             (write-byte (ldb (byte 8 40) u64) self)
             (write-byte (ldb (byte 8 32) u64) self)
             (write-byte (ldb (byte 8 24) u64) self)
             (write-byte (ldb (byte 8 16) u64) self)
             (write-byte (ldb (byte 8 8) u64) self)
             (write-byte (ldb (byte 8 0) u64) self))))))


(defmethod write-value ((type (eql :asciiz)) (self stream) string &key (array-size 0) (alignment 0) endian)
  (declare (ignore endian))
  (with-size-and-alignment-write
      alignment array-size string
      (progn
        (iterate 
         (for char in-string string)
         (write-char char self))
        (write-char #\Nul self))))

(defmethod write-value ((type (eql :nstring32)) (self stream) string &key (array-size 0) (alignment 0) endian)
  (with-size-and-alignment-write
      alignment array-size string
      (progn
        (ccase endian
               (:little
                (let ((u32 (length string)))
                  (write-byte (ldb (byte 8 0) u32) self)
                  (write-byte (ldb (byte 8 8) u32) self)
                  (write-byte (ldb (byte 8 16) u32) self)
                  (write-byte (ldb (byte 8 24) u32) self)))
                (:big
                 (let ((u32 (length string)))
                   (write-byte (ldb (byte 8 24) u32) self)
                   (write-byte (ldb (byte 8 16) u32) self)
                   (write-byte (ldb (byte 8 8) u32) self)
                   (write-byte (ldb (byte 8 0) u32) self))))
        (iterate 
         (for char in-string string)
         (write-char char self))
        (write-char #\Nul self))))
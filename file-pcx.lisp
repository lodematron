
(in-package :lodematron)

(define-binary-class pcx-header
    ((manufacturer  :u8) ;  resb    1       ; should always be 0ah
     (version       :u8) ; resb    1       ; (1)
     (encoding      :u8) ;  resb    1       ; should always be 01h
     (bits-per-pixel   :u8) ; resb    1       ; (2)
     (xmin           :u16) ; resw    1       ; image width = xmax-xmin
     (ymin           :u16) ; resw    1       ; image height = ymax-ymin
     (xmax           :u16) ; resw    1
     (ymax           :u16) ; resw    1
     (vertdpi        :u16) ; resw    1       ; (3)
     (palette        :u8 :array-size 48) ; resb    48      ; (4)
     (reserved       :u8) ; resb    1
     (colour-planes    :u8) ; resb    1       ; (5)
     (bytes-per-line   :u16) ; resw    1       ; (6)
     (palette-type    :u16) ;resw    1
     (hscrsize       :u16) ; resw    1       ; only supported by
     (vscrsize       :u16) ;   1       ; pc paintbrush iv or higher
     (filler         :u8 :array-size 56)))

(defun length-byte-p (byte)
    (= (logand byte #XA0) #XA0))

(defun length-byte (byte)
    (logand byte (lognot #XA0)))


(defun decode-run (stream array)
  (let ((length (read-byte stream)))
    (if (length-byte-p length)
        (let ((colour (read-byte stream)))
          (iterate
           (for index from 0 below (length-byte length))
           (vector-push colour array))
          (length-byte length))
        (progn
          (vector-push length array)
          1))))

(defun decode-row (stream array row-length)
  (let ((row-index 0))
    (iterate
     (while (< row-index row-length))
     (incf row-index (decode-run stream array)))))

(defun decode-rgb (stream row-length nrows &key red green blue)
  (iterate
   (for index from 0 below nrows)
   (decode-row stream red row-length)
   (decode-row stream green row-length)
   (decode-row stream blue row-length)))


(defun merge-arrays (&key red green blue)
  (assert (= (length red) (length green) (length blue)))
  (let ((result (make-colour-array (length red)  :fill-pointer 0)))
    (iterate
     (for r in-vector red)
     (for g in-vector green)
     (for b in-vector blue)
     (colour-vector-push (colour* (/ r 255.0) (/ g 255.0) (/ b 255.0) 1.0) result))
  result))

(defun extract-palette (stream)
   (let ((palette-pos (- (file-length stream) 768)))
     (file-position stream palette-pos)
;;     (format t "Reading palette from ~X~%" (file-position stream))
;;     (assert (= (read-byte stream) 192)) 
     (read-value :u8 stream :array-size 768)))

(defun decode-8bit (stream row-length nrows)
  (let ((index-array (make-array (* row-length nrows) :fill-pointer 0)))
    (iterate
     (for index from 0 below nrows)
     (decode-row stream index-array row-length))
    index-array))
             
(defun merge-array-and-palette (array palette)
  (let ((result (make-colour-array  (length array) :fill-pointer 0)))
    (iterate
      (for palette-index in-vector array)
      (colour-vector-push (colour*  (/ (aref palette (* 3 palette-index)) 255.0)
                                    (/ (aref palette (1+ (* 3 palette-index))) 255.0)
                                    (/ (aref palette (+ 2 (* 3 palette-index))) 255.0)
                                    1.0) 
                          result))
    result))


(defun parse-pcx-file (filename)
  (with-open-file (stream (merge-pathnames filename)  :direction :input :element-type '(unsigned-byte 8))     
    (let* ((header (read-value :pcx-header stream))
           (xmax (xmax-of header))
           (ymax (ymax-of header))
           (xmin (xmin-of header))
           (ymin (ymin-of header))
           (red-array (make-array (list (* (- xmax xmin) (- ymax ymin))) :element-type '(unsigned-byte 32) :fill-pointer 0))
           (blue-array (make-array (list (* (- xmax xmin) (- ymax ymin))) :element-type '(unsigned-byte 32) :fill-pointer 0))
           (green-array (make-array (list (* (- xmax xmin) (- ymax ymin))) :element-type '(unsigned-byte 32) :fill-pointer 0)))
      (format *debug-io* "PCX width ~A height ~A size ~A ~%"  (- xmax xmin) (- ymax ymin) (* (- xmax xmin) (- ymax ymin))) 
      (mixamesh:make-texture :width (- xmax xmin) :height (- ymax ymin) :colour-map
                    (case (bits-per-pixel-of header)
                      (1 (warn "Unsupported PCX Format"))
                      (4 (warn "Unsupported PCX Format"))
                      (8 (merge-array-and-palette (decode-8bit stream (- xmax xmin) (- ymax ymin)) (extract-palette stream)))
                      (24 (progn (decode-rgb stream (/ (bytes-per-line-of header)  (colour-planes-of header)) (- ymax ymin) 
                                             :red red-array :green green-array :blue blue-array)
                                 (merge-arrays :red red-array :green green-array :blue blue-array))))))))
    
    